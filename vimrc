execute pathogen#infect()
inoremap jk <ESC>
let mapleader = "<Space>"
syntax on
colorscheme darkest-space
filetype plugin indent on
autocmd vimenter * NERDTree
